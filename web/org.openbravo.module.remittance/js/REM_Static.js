isc.OBToolbar.addClassProperties({
  REM_Original_DELETE_BUTTON_PROPERTIES_action:
    isc.OBToolbar.DELETE_BUTTON_PROPERTIES.action
});

isc.addProperties(
  isc.OBToolbar.DELETE_BUTTON_PROPERTIES,
  isc.OBToolbar.DELETE_BUTTON_PROPERTIES,
  {
    action: function() {
      if (this.view.tabId === 'CEE05A3E6C3F4624A6444039AD362993') {
        var callback,
          remittanceLines = [],
          i,
          view = this.view,
          grid = view.viewGrid,
          selectedRecords = grid.getSelectedRecords();
        // collect the remittanceLines ids
        for (i = 0; i < selectedRecords.length; i++) {
          remittanceLines.push(selectedRecords[i].id);
        }

        // define the callback function which shows the result to the user
        callback = function(rpcResponse, data, rpcRequest) {
          var view = rpcRequest.clientContext.view;
          view.refresh();
          view.messageBar.setMessage(
            data.message.severity,
            data.message.title,
            data.message.text,
            [i]
          );
        };

        // and call the server
        OB.RemoteCallManager.call(
          'org.openbravo.module.remittance.process.DeleteRemittanceLineActionHandler',
          {
            remittanceLines: remittanceLines
          },
          {},
          callback,
          {
            view: view
          }
        );

        //this.view.dataSource.removeDataURL = OB.Application.contextUrl + 'org.openbravo.service.datasource/REM_RemittanceLine';
      } else {
        isc.OBToolbar.REM_Original_DELETE_BUTTON_PROPERTIES_action.call(
          this,
          arguments
        );
      }
    }
  }
);
