/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2013 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.ad_actionbutton;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.filter.RequestFilter;
import org.openbravo.base.filter.ValueListFilter;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.info.SelectorUtility;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SQLReturnObject;
import org.openbravo.erpCommon.utility.TableSQLData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.xmlEngine.XmlDocument;

public class SelectPayments extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  private static final RequestFilter filterYesNo = new ValueListFilter("Y", "N", "");

  private static final String[] colNames = { "documentNo", "businessPartner", "description",
      "paymentMethod", "paymentDate", "amount", "status", "rowkey" };

  private static final RequestFilter columnFilter = new ValueListFilter(colNames);
  private static final RequestFilter directionFilter = new ValueListFilter("asc", "desc");

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strRemittanceId = vars.getGlobalVariable("inpremRemittanceId",
          "REM_Remittance|SelectPayments", "");
      String strTabId = vars.getGlobalVariable("inpTabId", "REM_Remittance|Tab_ID");
      printPage(response, vars, strTabId, strRemittanceId);

    } else if (vars.commandIn("STRUCTURE")) {
      printGridStructure(response, vars);

    } else if (vars.commandIn("DATA")) {
      String action = vars.getStringParameter("action");
      log4j.debug("command DATA - action: " + action);
      if (!"getColumnTotals".equals(action)) {
        String strRemittanceId = vars.getGlobalVariable("inpremRemittanceId",
            "REM_Remittance|SelectPayments", "");
        Boolean showAlternativePM = "Y".equals(vars.getStringParameter(
            "inpAlternativePaymentMethod", filterYesNo));

        String strNewFilter = vars.getStringParameter("newFilter");
        String strOffset = vars.getStringParameter("offset");
        String strPageSize = vars.getStringParameter("page_size");
        String strSortCols = vars.getInStringParameter("sort_cols", columnFilter);
        String strSortDirs = vars.getInStringParameter("sort_dirs", directionFilter);

        printGridData(response, vars, strRemittanceId, strSortCols, strSortDirs, strOffset,
            strPageSize, strNewFilter, showAlternativePM);
      } else {
        // When mouse over a float column in a dojo grid this action is performed automatically
        XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
            "org/openbravo/erpCommon/utility/DataGridTotal").createXmlDocument();
        xmlDocument.setParameter("total", "0");
        response.setContentType("text/xml; charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter out = response.getWriter();
        out.println(xmlDocument.print());
        out.close();
      }

    } else if (vars.commandIn("SAVE")) {
      String strRemittanceId = vars.getGlobalVariable("inpremRemittanceId",
          "REM_Remittance|SelectPayments", "");
      String strSelectedPaymentsIds = vars.getRequestGlobalVariable("inpSelectedRowList", "");
      String strTabId = vars.getGlobalVariable("inpTabId", "REM_Remittance|Tab_ID");
      saveAndCloseWindow(response, vars, strTabId, strRemittanceId, strSelectedPaymentsIds);

    } else {
      pageError(response);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strTabId,
      String strRemittance) throws IOException, ServletException {

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/remittance/ad_actionbutton/SelectPayments").createXmlDocument();

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("tabId", strTabId);

    OBError myMessage = vars.getMessage("REM_Remittance|SelectPayments");
    vars.removeMessage("REM_Remittance|SelectPayments");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printGridStructure(HttpServletResponse response, VariablesSecureApp vars)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: print page structure");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/utility/DataGridStructure").createXmlDocument();

    SQLReturnObject[] data = getHeaders(vars);
    String type = "Hidden";
    String title = "";
    String description = "";

    xmlDocument.setParameter("type", type);
    xmlDocument.setParameter("title", title);
    xmlDocument.setParameter("description", description);
    xmlDocument.setData("structure1", data);
    xmlDocument.setParameter("backendPageSize", String.valueOf(TableSQLData.maxRowsPerGridPage));
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private SQLReturnObject[] getHeaders(VariablesSecureApp vars) {
    SQLReturnObject[] data = null;
    Vector<SQLReturnObject> vAux = new Vector<SQLReturnObject>();
    String[] colWidths = { "90", "190", "190", "110", "90", "90", "110", "0" };
    String[] colLabelsFormat = { "string", "string", "string", "string", "string", "float",
        "string", "string" };
    // Message search key to translate the column
    String[] colLabels = { "documentNo", "businessPartner", "description", "paymentMethod",
        "paymentDate", "amount", "status", "rowkey" };

    for (int i = 0; i < colNames.length; i++) {
      SQLReturnObject dataAux = new SQLReturnObject();
      dataAux.setData("columnname", colNames[i]);
      dataAux.setData("gridcolumnname", colNames[i]);
      dataAux.setData("adReferenceId", "AD_Reference_ID");
      dataAux.setData("adReferenceValueId", "AD_ReferenceValue_ID");
      dataAux.setData("isidentifier", (colNames[i].equals("rowkey") ? "true" : "false"));
      dataAux.setData("iskey", (colNames[i].equals("rowkey") ? "true" : "false"));
      dataAux
          .setData("isvisible",
              (colNames[i].endsWith("_ID") || colNames[i].equalsIgnoreCase("rowkey") ? "false"
                  : "true"));
      String name = Utility
          .messageBD(this, "REM_" + colLabels[i].toUpperCase(), vars.getLanguage());
      dataAux.setData("name", name);
      dataAux.setData("type", colLabelsFormat[i]);
      dataAux.setData("width", colWidths[i]);
      vAux.addElement(dataAux);
    }
    data = new SQLReturnObject[vAux.size()];
    vAux.copyInto(data);
    return data;
  }

  private void printGridData(HttpServletResponse response, VariablesSecureApp vars,
      String strRemittance, String strOrderCols, String strOrderDirs, String strOffset,
      String strPageSize, String strNewFilter, boolean showAlternativePM) throws IOException,
      ServletException {
    log4j.debug("Output: print page rows");

    SQLReturnObject[] headers = getHeaders(vars);
    String type = "Hidden";
    String title = "";
    String description = "";
    String strNumRows = "0";
    int page = 0;
    int offset = Integer.valueOf(strOffset).intValue();
    int pageSize = Integer.valueOf(strPageSize).intValue();
    List<FIN_Payment> gridPayments = null;

    Remittance remittance = OBDal.getInstance().get(Remittance.class, strRemittance);
    String strOrgId = remittance.getOrganization().getId();
    String strPaymentMethodId = remittance.getRemittanceType().getPaymentMethod().getId();
    String strFinancialAccountId = remittance.getFinancialAccount().getId();
    String isReceipt = ""; // Y: sales, N: purchase, B: both
    if (remittance.getRemittanceType().getPaymentMethod().isPayinAllow()
        && remittance.getRemittanceType().getPaymentMethod().isPayoutAllow()) {
      isReceipt = "B";
    } else if (remittance.getRemittanceType().getPaymentMethod().isPayinAllow()) {
      isReceipt = "Y";
    } else if (remittance.getRemittanceType().getPaymentMethod().isPayoutAllow()) {
      isReceipt = "N";
    }

    if (headers != null) {
      try {
        // build sql orderBy clause from parameters
        String strOrderBy = SelectorUtility.buildOrderByClause(strOrderCols, strOrderDirs);
        String[] orderByClause = strOrderBy.split(" ");
        String strOrderByProperty = orderByClause[0];
        String strAscDesc = orderByClause[1];

        page = TableSQLData
            .calcAndGetBackendPage(vars, "REM_Remittance_SelectPayments.currentPage");
        int oldOffset = offset;
        offset = (page * TableSQLData.maxRowsPerGridPage) + offset;
        log4j.debug("relativeOffset: " + oldOffset + " absoluteOffset: " + offset);

        if (strNewFilter.equals("1") || strNewFilter.equals("")) {
          // New filter or first load
          gridPayments = getPaymentsAvaiableForRemittance(strOrgId, showAlternativePM ? null
              : strPaymentMethodId, strFinancialAccountId, null, null, offset,
              TableSQLData.maxRowsPerGridPage, null, null, isReceipt, strRemittance);

          strNumRows = Integer.toString(gridPayments.size());
          vars.setSessionValue("REM_Remittance_SelectPayments.numrows", strNumRows);
        } else {
          strNumRows = vars.getSessionValue("REM_Remittance_SelectPayments.numrows");
        }

        gridPayments = getPaymentsAvaiableForRemittance(strOrgId, showAlternativePM ? null
            : strPaymentMethodId, strFinancialAccountId, null, null, offset, pageSize,
            strOrderByProperty, strAscDesc, isReceipt, strRemittance);

      } catch (ServletException e) {
        log4j.error("Error in print page data: ", e);
        OBError myError = Utility.translateError(this, vars, vars.getLanguage(), e.getMessage());
        if (!myError.isConnectionAvailable()) {
          bdErrorAjax(response, "Error", "Connection Error", "No database connection");
          return;
        } else {
          type = myError.getType();
          title = myError.getTitle();
          if (!myError.getMessage().startsWith("<![CDATA["))
            description = "<![CDATA[" + myError.getMessage() + "]]>";
          else
            description = myError.getMessage();
        }
      } catch (Exception e) {
        if (log4j.isDebugEnabled())
          log4j.debug("Error obtaining rows data");
        type = "Error";
        title = "Error";
        if (e.getMessage().startsWith("<![CDATA[")) {
          description = "<![CDATA[" + e.getMessage() + "]]>";
        } else {
          description = e.getMessage();
        }
        log4j.error("Error in print grid data: ", e);
      }
    }

    if (!type.startsWith("<![CDATA["))
      type = "<![CDATA[" + type + "]]>";
    if (!title.startsWith("<![CDATA["))
      title = "<![CDATA[" + title + "]]>";
    if (!description.startsWith("<![CDATA["))
      description = "<![CDATA[" + description + "]]>";
    StringBuffer strRowsData = new StringBuffer();
    strRowsData.append("<xml-data>\n");
    strRowsData.append("  <status>\n");
    strRowsData.append("    <type>").append(type).append("</type>\n");
    strRowsData.append("    <title>").append(title).append("</title>\n");
    strRowsData.append("    <description>").append(description).append("</description>\n");
    strRowsData.append("  </status>\n");
    strRowsData.append("  <rows numRows=\"").append(strNumRows).append("\">\n");

    if (gridPayments != null && gridPayments.size() > 0) {

      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
      dfs.setDecimalSeparator(vars.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
      dfs.setGroupingSeparator(vars.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
      final DecimalFormat numberFormat = new DecimalFormat(
          vars.getSessionValue("#AD_ReportNumberFormat"), dfs);

      for (FIN_Payment pay : gridPayments) {
        strRowsData.append("    <tr>\n");
        for (int k = 0; k < headers.length; k++) {
          strRowsData.append("      <td><![CDATA[");
          // "documentNo", "businessPartner", "description", "paymentMethod",
          // "paymentDate", "amount", "status", "rowkey"
          String columnData = "";
          switch (k) {
          case 0: // documentNo
            columnData = pay.getDocumentNo();
            break;
          case 1: // businessPartner
            try {
              OBContext.setAdminMode(true);
              BusinessPartner bp = pay.getBusinessPartner();
              if (bp == null) {
                bp = getMultiBPartner(pay.getFINPaymentDetailList());
              }
              columnData = (bp == null) ? "" : bp.getIdentifier();
            } finally {
              OBContext.restorePreviousMode();
            }
            break;
          case 2: // description
            if (pay.getDescription() != null)
              columnData = pay.getDescription();
            break;
          case 3: // paymentMethod
            columnData = pay.getPaymentMethod().getIdentifier();
            break;
          case 4: // paymentDate
            columnData = Utility.formatDate(pay.getPaymentDate(), vars.getJavaDateFormat());
            break;
          case 5: // amount
            columnData = numberFormat.format(pay.getAmount());
            break;
          case 6: // status
            if (("RPR".equals(pay.getStatus())) || ("RPAE".equals(pay.getStatus()))) {
              columnData = Utility.getListValueName("FIN_Payment status", pay.getStatus(),
                  vars.getLanguage());
            } else {
              columnData = "";
            }
            break;
          case 7: // rowkey
            columnData = pay.getId().toString();
            break;
          default: // invalid
            log4j.error("Invalid column");
            break;
          }

          if (columnData != "") {
            if (headers[k].getField("adReferenceId").equals("32"))
              strRowsData.append(strReplaceWith).append("/images/");
            strRowsData.append(columnData.replaceAll("<b>", "").replaceAll("<B>", "")
                .replaceAll("</b>", "").replaceAll("</B>", "").replaceAll("<i>", "")
                .replaceAll("<I>", "").replaceAll("</i>", "").replaceAll("</I>", "")
                .replaceAll("<p>", "&nbsp;").replaceAll("<P>", "&nbsp;")
                .replaceAll("<br>", "&nbsp;").replaceAll("<BR>", "&nbsp;"));
          } else {
            if (headers[k].getField("adReferenceId").equals("32")) {
              strRowsData.append(strReplaceWith).append("/images/blank.gif");
            } else
              strRowsData.append("&nbsp;");
          }
          strRowsData.append("]]></td>\n");
        }
        strRowsData.append("    </tr>\n");
      }
    }
    strRowsData.append("  </rows>\n");
    strRowsData.append("</xml-data>\n");

    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter out = response.getWriter();
    out.print(strRowsData.toString());
    out.close();
  }

  /**
   * This method returns list of Payments that are in Awaiting Execution status and filtered by the
   * following parameters.
   * 
   * @param organizationId
   *          Organization
   * @param paymentMethodId
   *          Payment Method used for the payment.
   * @param financialAccountId
   *          Financial Account used for the payment.
   * @param dateFrom
   *          Optional. Filters payments made after the specified date.
   * @param dateTo
   *          Optional. Filters payments made before the specified date.
   * @param offset
   *          Starting register number.
   * @param pageSize
   *          Limited the max number of results.
   * @param strOrderByProperty
   *          Property used for ordering the results.
   * @param strAscDesc
   *          if true order by asc, if false order by desc
   * @param isReceipt
   *          Y: sales, N: purchase, B: both
   * @param remittanceId
   *          Remittance identifier
   * @return Filtered Payment list.
   */
  private List<FIN_Payment> getPaymentsAvaiableForRemittance(String organizationId,
      String paymentMethodId, String financialAccountId, Date dateFrom, Date dateTo, int offset,
      int pageSize, String strOrderByProperty, String strAscDesc, String isReceipt,
      String remittanceId) {
    if ("".equals(isReceipt)) {
      // If it does not allow IN/OUT just return empty list
      return new ArrayList<FIN_Payment>();
    }
    OBContext.setAdminMode();
    try {

      Remittance remittance = OBDal.getInstance().get(Remittance.class, remittanceId);

      FIN_FinancialAccount obFinAccount = OBDal.getInstance().get(FIN_FinancialAccount.class,
          financialAccountId);

      OBCriteria<FIN_Payment> obcPayment = OBDal.getInstance().createCriteria(FIN_Payment.class);
      obcPayment.add(Restrictions.in("organization.id", OBContext.getOBContext()
          .getOrganizationStructureProvider().getChildTree(organizationId, true)));
      obcPayment.add(Restrictions.or(Restrictions.eq(FIN_Payment.PROPERTY_STATUS, "RPAE"),
          Restrictions.eq(FIN_Payment.PROPERTY_STATUS, "RPR")));
      if (paymentMethodId != null) {
        obcPayment.add(Restrictions.eq(FIN_Payment.PROPERTY_PAYMENTMETHOD,
            OBDal.getInstance().get(FIN_PaymentMethod.class, paymentMethodId)));
      }
      obcPayment.add(Restrictions.eq(FIN_Payment.PROPERTY_ACCOUNT, obFinAccount));
      if ("Y".equals(isReceipt)) {
        obcPayment.add(Restrictions.eq(FIN_Payment.PROPERTY_RECEIPT, true));
      } else if ("N".equals(isReceipt)) {
        obcPayment.add(Restrictions.eq(FIN_Payment.PROPERTY_RECEIPT, false));
      } else if ("B".equals(isReceipt) || isReceipt.isEmpty()) {
        // take all
      }
      if (dateFrom != null)
        obcPayment.add(Restrictions.ge(FIN_Payment.PROPERTY_PAYMENTDATE, dateFrom));
      if (dateTo != null)
        obcPayment.add(Restrictions.lt(FIN_Payment.PROPERTY_PAYMENTDATE, dateTo));

      boolean ascDesc = true;
      if (strAscDesc != null && !strAscDesc.isEmpty())
        ascDesc = "asc".equalsIgnoreCase(strAscDesc);
      if (strOrderByProperty != null && !strOrderByProperty.isEmpty())
        obcPayment.addOrderBy(strOrderByProperty, ascDesc);
      obcPayment.setFirstResult(offset);
      obcPayment.setMaxResults(pageSize);

      List<FIN_Payment> filterPayments = obcPayment.list();

      // Exclude payments already included in the remittance
      List<FIN_Payment> toRemoveFromList = new ArrayList<FIN_Payment>();
      for (RemittanceLine rl : remittance.getREMRemittanceLineList()) {
        toRemoveFromList.add(rl.getPayment());
      }
      filterPayments.removeAll(toRemoveFromList);
      return filterPayments;

    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void saveAndCloseWindow(HttpServletResponse response, VariablesSecureApp vars,
      String strTabId, String strRemittance, String strSelectedPaymentsIds) throws IOException,
      ServletException {
    Remittance remittance = OBDal.getInstance().get(Remittance.class, strRemittance);
    List<FIN_Payment> payments = getOBObjectListFromString(FIN_Payment.class,
        strSelectedPaymentsIds);
    OBError message = new OBError();
    message.setType("Success");
    message.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));

    try {
      Long maxLine = 0l;
      for (FIN_Payment p : payments) {
        // Get max line number
        final OBCriteria<RemittanceLine> obc = OBDal.getInstance().createCriteria(
            RemittanceLine.class);
        obc.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE, remittance));
        obc.addOrderBy(FIN_FinaccTransaction.PROPERTY_LINENO, false);
        obc.setMaxResults(1);
        if (obc.list() != null && obc.list().size() > 0) {
          maxLine = obc.list().get(0).getLineNo();
        }

        final RemittanceLine remLine = OBProvider.getInstance().get(RemittanceLine.class);
        remLine.setAmount(p.getAmount());
        remLine.setPayment(p);
        remLine.setRemittance(remittance);
        remLine.setOrganization(remittance.getOrganization());
        remLine.setLineNo(maxLine + 10);
        remLine.setOrigstatus(p.getStatus());

        OBDal.getInstance().save(remLine);
        OBDal.getInstance().flush();
      }

    } catch (Exception ex) {
      message = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
      log4j.error(ex);
      if (!message.isConnectionAvailable()) {
        bdErrorConnection(response);
        return;
      }
    }

    vars.setMessage(strTabId, message);
    printPageClosePopUpAndRefreshParent(response, vars);
  }

  /**
   * Parses the string of comma separated id's to return a List with the BaseOBObjects of the given
   * class. If there is an invalid id a null value is added to the List. Added from OBDao.java (3.0)
   * for 2.50 compatibility
   * 
   * @param t
   *          class of the BaseOBObject the id's belong to
   * @param _IDs
   *          String containing the comma separated list of id's
   * @return a List object containing the parsed OBObjects
   */
  private static <T extends BaseOBObject> List<T> getOBObjectListFromString(Class<T> t, String _IDs) {
    String strBaseOBOBjectIDs = _IDs;
    final List<T> baseOBObjectList = new ArrayList<T>();
    if (strBaseOBOBjectIDs.startsWith("(")) {
      strBaseOBOBjectIDs = strBaseOBOBjectIDs.substring(1, strBaseOBOBjectIDs.length() - 1);
    }
    if (!strBaseOBOBjectIDs.equals("")) {
      strBaseOBOBjectIDs = StringUtils.remove(strBaseOBOBjectIDs, "'");
      StringTokenizer st = new StringTokenizer(strBaseOBOBjectIDs, ",", false);
      while (st.hasMoreTokens()) {
        String strBaseOBObjectID = st.nextToken().trim();
        baseOBObjectList.add((T) OBDal.getInstance().get(t, strBaseOBObjectID));
      }
    }
    return baseOBObjectList;
  }

  private BusinessPartner getMultiBPartner(List<FIN_PaymentDetail> paymentDetailList) {
    String previousBPId = null;
    String currentBPId = null;
    for (FIN_PaymentDetail pd : paymentDetailList) {
      for (FIN_PaymentScheduleDetail psd : pd.getFINPaymentScheduleDetailList()) {
        if (psd.getInvoicePaymentSchedule() != null) { // Invoice
          currentBPId = psd.getInvoicePaymentSchedule().getInvoice().getBusinessPartner().getId();
          if (!currentBPId.equals(previousBPId) && previousBPId != null) {
            return null;
          } else {
            previousBPId = currentBPId;
          }
        }
        if (psd.getOrderPaymentSchedule() != null) { // Order
          currentBPId = psd.getOrderPaymentSchedule().getOrder().getBusinessPartner().getId();
          if (!currentBPId.equals(previousBPId) && previousBPId != null) {
            return null;
          } else {
            previousBPId = currentBPId;
          }
        }
      }
    }
    return currentBPId != null ? OBDal.getInstance().get(BusinessPartner.class, currentBPId) : null;
  }

  public String getServletInfo() {
    return "Servlet that presents que multiple payment seeker";
  } // end of getServletInfo() method

}