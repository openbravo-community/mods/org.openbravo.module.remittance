/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.remittance.ad_callouts;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;
import org.openbravo.module.remittance.RemittanceType;

public class REM_RemittanceType_FinAccount extends SimpleCallout {

  @Override
  protected void execute(CalloutInfo info) throws ServletException {

    String strRemittanceType = info.getStringParameter("inpremRemittanceTypeId",
        IsIDFilter.instance);
    String strOrgId = info.getStringParameter("inpadOrgId", IsIDFilter.instance);

    RemittanceType obRemType = OBDal.getInstance().get(RemittanceType.class, strRemittanceType);

    final OBCriteria<FinAccPaymentMethod> obc = OBDal.getInstance().createCriteria(
        FinAccPaymentMethod.class);
    obc.createAlias(FinAccPaymentMethod.PROPERTY_ACCOUNT, "acc");
    obc.add(Restrictions.in("acc.organization.id", OBContext.getOBContext()
        .getOrganizationStructureProvider().getNaturalTree(strOrgId)));
    obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD,
        obRemType.getPaymentMethod()));
    obc.setFilterOnReadableOrganization(false);
    info.addResult("inpisdiscount", obRemType.isRemitForDiscount() ? "Y" : "N");
    info.addSelect("inpfinFinancialAccountId");
    for (FinAccPaymentMethod fapm : obc.list()) {
      info.addSelectResult(fapm.getAccount().getId(), fapm.getAccount().getIdentifier());
    }
    info.endSelect();

  }
}
