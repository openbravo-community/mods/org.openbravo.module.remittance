/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.executionprocess;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import org.openbravo.advpaymentmngt.utility.FIN_PaymentExecutionProcess;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.PaymentRun;
import org.openbravo.model.financialmgmt.payment.PaymentRunParameter;
import org.openbravo.model.financialmgmt.payment.PaymentRunPayment;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.process.REM_AddRemittance;

public class InRemittance implements FIN_PaymentExecutionProcess {

  @Override
  public OBError execute(PaymentRun paymentRun) throws ServletException {
    OBError result = new OBError();
    result.setMessage("@Success@");
    List<PaymentRunParameter> parameters = paymentRun.getFinancialMgmtPaymentRunParameterList();
    boolean isWriteOff = false;
    boolean isInRemittance = false;
    try {
      for (PaymentRunParameter parameter : parameters) {
        // TODO: Review parameters. Text is not required now a days
        if ("IN".equalsIgnoreCase(parameter.getPaymentExecutionProcessParameter()
            .getParameterType())) {
          if (!("TEXT".equalsIgnoreCase(parameter.getPaymentExecutionProcessParameter()
              .getInputType()))) {
            if ("IsInRemittance".equals(parameter.getPaymentExecutionProcessParameter()
                .getSearchKey())) {
              isInRemittance = parameter.isValueOfTheCheck();
            }
            if ("IsWriteOff".equals(parameter.getPaymentExecutionProcessParameter().getSearchKey())) {
              isWriteOff = parameter.isValueOfTheCheck();
            }
          }
        }
      }

      for (PaymentRunPayment paymentRunPayment : paymentRun.getFinancialMgmtPaymentRunPaymentList()) {
        FIN_Payment payment = paymentRunPayment.getPayment();
        HashMap<String, String> rem = REM_AddRemittance.existLineInAnyRemittance(payment);
        if (!rem.isEmpty()) {
          result.setType("Error");
          result.setMessage(String.format(FIN_Utility.messageBD("REM_JustOnceInRemittanceLines"),
              rem.get("documentno"), rem.get("lineno")));
          return result;
        }
        if (isInRemittance && isWriteOff) {
          result.setType("Error");
          result.setMessage("@REM_JustOneAction@");
          return result;
        }
        if (!isInRemittance && !isWriteOff) {
          result.setType("Error");
          result.setMessage("@REM_AtLeastOneAction@");
          return result;
        }
        if (isInRemittance) {
          Remittance remittance = REM_AddRemittance.getRemittance(payment);
          if (remittance != null) {
            REM_AddRemittance.newRemittanceLine(remittance, payment);
          } else {
            result.setType("Error");
            result.setMessage("@REM_NoRemittance@");
            return result;
          }
          result.setMessage(String.format(FIN_Utility.messageBD("REM_AddedToRemittance"),
              remittance.getIdentifier()));
        }
        if (isWriteOff) {
          List<FIN_PaymentDetail> paymentDetails = payment.getFINPaymentDetailList();
          payment.setProcessed(false);
          OBDal.getInstance().save(payment);
          OBDal.getInstance().flush();

          for (FIN_PaymentDetail pd : paymentDetails) {
            pd.setWriteoffAmount(pd.getAmount().add(payment.getWriteoffAmount()));
            pd.setAmount(BigDecimal.ZERO);
            OBDal.getInstance().save(pd);

            for (FIN_PaymentScheduleDetail pdDetail : pd.getFINPaymentScheduleDetailList()) {
              pdDetail.setWriteoffAmount(pd.getWriteoffAmount().add(pd.getAmount()));
              pdDetail.setAmount(BigDecimal.ZERO);
              OBDal.getInstance().save(pdDetail);
            }

          }
          payment.setWriteoffAmount(payment.getAmount().add(payment.getWriteoffAmount()));
          payment.setAmount(BigDecimal.ZERO);
          OBDal.getInstance().save(payment);
          OBDal.getInstance().flush();
          payment.setStatus(payment.isReceipt() ? "RPR" : "PPM");
          payment.setProcessed(true);

          OBDal.getInstance().save(payment);
          OBDal.getInstance().flush();
        }
        paymentRunPayment.setResult("S");
        paymentRun.setStatus("PE");
        OBDal.getInstance().save(paymentRun);
        OBDal.getInstance().save(paymentRunPayment.getPayment());
        OBDal.getInstance().save(paymentRunPayment);
      }
    } catch (NumberFormatException e) {
      result.setType("Error");
      result.setMessage("@APRM_NotValidNumber@");
      OBDal.getInstance().rollbackAndClose();
      return result;
    } catch (Exception e) {
      result.setType("Error");
      result.setMessage(FIN_Utility.getExceptionMessage(e));
      OBDal.getInstance().rollbackAndClose();
      return result;
    }
    paymentRun.setStatus("E");
    OBDal.getInstance().save(paymentRun);
    OBDal.getInstance().flush();
    result.setType("Success");
    return result;
  }
}
