/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.RemittanceLineCancel;
import org.openbravo.scheduling.ProcessBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class REM_RemittanceSettleUndo implements org.openbravo.scheduling.Process {
  private static final Logger log4j = LoggerFactory.getLogger(REM_RemittanceSettleUndo.class);
  private static AdvPaymentMngtDao dao;
  public static final String STATUS_SENT = "REM_SENT";
  public static final String STATUS_PAYMENTRECEIVED = "RPR";
  public static final String STATUS_CANCELLED = "REM_CANCEL";
  public static final String STATUS_DEPOSITED = "RDNC";
  public static final String STATUS_WITHDRAWN = "PWNC";
  public static final String STATUS_AWAITING = "RPAE";

  public void execute(ProcessBundle bundle) throws Exception {
    dao = new AdvPaymentMngtDao();
    boolean wasPaid = false;
    OBError msg = new OBError();
    msg.setType("Success");
    msg.setTitle(Utility.messageBD(bundle.getConnection(), "Success", bundle.getContext()
        .getLanguage()));

    OBContext.setAdminMode();
    try {
      final String recordID = (String) bundle.getParams().get("REM_Remittanceline_Cancel_ID");
      final RemittanceLineCancel remittanceLineCancel = dao.getObject(RemittanceLineCancel.class,
          recordID);
      final VariablesSecureApp vars = bundle.getContext().toVars();
      final ConnectionProvider conProvider = bundle.getConnection();
      if ("Y".equals(remittanceLineCancel.getPosted())) {
        msg.setType("Error");
        msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
        msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
            "@PostedDocument@"));
        bundle.setResult(msg);
        return;
      }

      FIN_Payment payment = remittanceLineCancel.getPayment();
      if (getFINFinaccTransactionList(payment).size() > 0) {
        // Check lines not payment
        msg.setType("Error");
        msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
        msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
            "@REM_RemittanceCancelInFA@"));
        bundle.setResult(msg);
        return;
      }
      OBCriteria<RemittanceLine> rmLineList = OBDal.getInstance().createCriteria(
          RemittanceLine.class);
      rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE,
          remittanceLineCancel.getRemittance()));
      rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_PAYMENT, payment));
      rmLineList.setMaxResults(1);
      RemittanceLine remLine = (RemittanceLine) rmLineList.uniqueResult();
      Remittance remittance = remLine.getRemittance();

      if (remLine != null) {
        if (remittance.getRemittanceType().isRemitForDiscount()) {
          if (remLine.getOrigstatus() == null) {
            wasPaid = false;
          } else {
            if ((FIN_Utility.seqnumberpaymentstatus(remLine.getOrigstatus())) <= (FIN_Utility
                .seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine.getPayment())))
                & ((FIN_Utility.invoicePaymentStatus(remLine.getPayment()).equals(payment
                    .isReceipt() ? STATUS_DEPOSITED : STATUS_WITHDRAWN)) | (FIN_Utility
                    .invoicePaymentStatus(remLine.getPayment()).equals(STATUS_CANCELLED)))) {
              wasPaid = false;
            } else if (remLine.getOrigstatus().equals(STATUS_AWAITING)) {
              wasPaid = false;
            } else {
              wasPaid = (FIN_Utility.seqnumberpaymentstatus(remLine.getOrigstatus())) <= (FIN_Utility
                  .seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine.getPayment())));
            }

          }
        } else {
          if (remLine.getOrigstatus() == null) {
            if (FIN_Utility.invoicePaymentStatus(remLine.getPayment()).equals(
                payment.isReceipt() ? STATUS_DEPOSITED : STATUS_WITHDRAWN)) {
              wasPaid = true;
            } else {
              wasPaid = false;
            }
          } else {
            if ((FIN_Utility.seqnumberpaymentstatus(remLine.getOrigstatus())) == (FIN_Utility
                .seqnumberpaymentstatus(payment.getStatus()))
                & (FIN_Utility.seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine
                    .getPayment())) <= FIN_Utility
                    .seqnumberpaymentstatus(payment.isReceipt() ? STATUS_DEPOSITED
                        : STATUS_WITHDRAWN))) {
              wasPaid = true;

            } else if (remLine.getOrigstatus().equals(STATUS_AWAITING)) {
              wasPaid = false;
            } else {
              wasPaid = (FIN_Utility.seqnumberpaymentstatus(remLine.getOrigstatus())) > (FIN_Utility
                  .seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine.getPayment())));
            }
          }

        }
      }

      OBCriteria<RemittanceLine> obc = OBDal.getInstance().createCriteria(RemittanceLine.class);
      obc.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE,
          remittanceLineCancel.getRemittance()));
      obc.add(Restrictions.eq(RemittanceLine.PROPERTY_PAYMENT, remittanceLineCancel.getPayment()));
      obc.setMaxResults(1);
      RemittanceLine line = (RemittanceLine) obc.uniqueResult();
      if ("Y".equals(payment.getPosted()) && !STATUS_PAYMENTRECEIVED.equals(line.getOrigstatus())) {
        msg.setType("Error");
        msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
        msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
            "@REM_PostedPayment@"));
        bundle.setResult(msg);
        return;
      }
      payment.setStatus(STATUS_SENT);
      OBDal.getInstance().save(payment);
      OBDal.getInstance().flush();
      remittanceLineCancel.setProcessed(false);
      OBDal.getInstance().save(remittanceLineCancel);
      OBDal.getInstance().flush();
      OBDal.getInstance().remove(remittanceLineCancel);
      OBDal.getInstance().flush();

      if (!wasPaid) {
        // Update Payment Monitor and Customer Credit
        try {
          OBContext.setAdminMode(true);
          BusinessPartner businessPartner = payment.getBusinessPartner();
          boolean isReceipt = payment.isReceipt();
          // When credit is used (consumed) we compensate so_creditused as this amount is already
          // included in the payment details. Credit consumed should not affect to so_creditused
          if (payment.getGeneratedCredit().compareTo(BigDecimal.ZERO) == 0
              && payment.getUsedCredit().compareTo(BigDecimal.ZERO) != 0) {
            if (isReceipt) {
              decreaseCustomerCredit(businessPartner, payment.getUsedCredit());
            } else {
              increaseCustomerCredit(businessPartner, payment.getUsedCredit());
            }
          }
          for (FIN_PaymentDetail paymentDetail : payment.getFINPaymentDetailList()) {
            for (FIN_PaymentScheduleDetail paymentScheduleDetail : paymentDetail
                .getFINPaymentScheduleDetailList()) {
              BigDecimal amount = paymentScheduleDetail.getAmount().add(
                  paymentScheduleDetail.getWriteoffAmount());
              if (paymentScheduleDetail.getInvoicePaymentSchedule() != null) {
                // BP SO_CreditUsed
                businessPartner = paymentScheduleDetail.getInvoicePaymentSchedule().getInvoice()
                    .getBusinessPartner();
                if (isReceipt) {
                  increaseCustomerCredit(businessPartner, amount);
                } else {
                  decreaseCustomerCredit(businessPartner, amount);
                }
                FIN_AddPayment.updatePaymentScheduleAmounts(paymentDetail, paymentScheduleDetail
                    .getInvoicePaymentSchedule(), paymentScheduleDetail.getAmount().negate(),
                    paymentScheduleDetail.getWriteoffAmount().negate());
              }
              if (paymentScheduleDetail.getOrderPaymentSchedule() != null) {
                FIN_AddPayment.updatePaymentScheduleAmounts(paymentDetail, paymentScheduleDetail
                    .getOrderPaymentSchedule(), paymentScheduleDetail.getAmount().negate(),
                    paymentScheduleDetail.getWriteoffAmount().negate());
              }
              // when generating credit for a BP SO_CreditUsed is also updated
              if (paymentScheduleDetail.getInvoicePaymentSchedule() == null
                  && paymentScheduleDetail.getOrderPaymentSchedule() == null
                  && paymentScheduleDetail.getPaymentDetails().getGLItem() == null) {
                // BP SO_CreditUsed
                if (isReceipt) {
                  increaseCustomerCredit(businessPartner, amount);
                } else {
                  decreaseCustomerCredit(businessPartner, amount);
                }
              }
              paymentScheduleDetail.setInvoicePaid(false);
              OBDal.getInstance().save(paymentScheduleDetail);
            }
          }
          OBDal.getInstance().save(payment);
          OBDal.getInstance().flush();
        } finally {
          OBContext.restorePreviousMode();
        }
      }
      bundle.setResult(msg);
    } catch (final Exception e) {
      log4j.error("Error in execute: ", e);
      msg.setType("Error");
      msg.setTitle(Utility.messageBD(bundle.getConnection(), "Error", bundle.getContext()
          .getLanguage()));
      msg.setMessage(FIN_Utility.getExceptionMessage(e));
      bundle.setResult(msg);
      OBDal.getInstance().rollbackAndClose();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void updateCustomerCredit(BusinessPartner businessPartner, BigDecimal amount, boolean add) {
    BigDecimal creditUsed = businessPartner.getCreditUsed();
    if (add) {
      creditUsed = creditUsed.add(amount);
    } else {
      creditUsed = creditUsed.subtract(amount);
    }
    businessPartner.setCreditUsed(creditUsed);
    OBDal.getInstance().save(businessPartner);
    OBDal.getInstance().flush();
  }

  private void increaseCustomerCredit(BusinessPartner businessPartner, BigDecimal amount) {
    updateCustomerCredit(businessPartner, amount, true);
  }

  private void decreaseCustomerCredit(BusinessPartner businessPartner, BigDecimal amount) {
    updateCustomerCredit(businessPartner, amount, false);
  }

  private List<FIN_FinaccTransaction> getFINFinaccTransactionList(FIN_Payment payment) {
    List<FIN_FinaccTransaction> list = new ArrayList<FIN_FinaccTransaction>();
    OBContext.setAdminMode();
    try {
      OBCriteria<FIN_FinaccTransaction> obcFAT = OBDal.getInstance().createCriteria(
          FIN_FinaccTransaction.class);
      obcFAT.setFilterOnReadableClients(false);
      obcFAT.setFilterOnReadableOrganization(false);
      obcFAT.add(Restrictions.eq(FIN_FinaccTransaction.PROPERTY_FINPAYMENT, payment));
      list = obcFAT.list();
    } finally {
      OBContext.restorePreviousMode();
    }
    return list;
  }
}
