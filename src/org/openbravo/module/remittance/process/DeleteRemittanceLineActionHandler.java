/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.process;

import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.module.remittance.RemittanceLine;

public class DeleteRemittanceLineActionHandler extends BaseActionHandler {

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    try {
      // get the data as json
      final JSONObject jsonData = new JSONObject(data);
      final JSONArray remittanceLineIds = jsonData.getJSONArray("remittanceLines");
      // iterate over the remittance line ids
      for (int i = 0; i < remittanceLineIds.length(); i++) {
        final String remittanceLineId = remittanceLineIds.getString(i);
        // get the remittance line
        final RemittanceLine remittanceLine = OBDal.getInstance().get(RemittanceLine.class,
            remittanceLineId);
        // delete it
        OBDal.getInstance().remove(remittanceLine);
        OBDal.getInstance().flush();
      }
      // create the result
      JSONObject json = new JSONObject();
      JSONObject message = new JSONObject();
      message.put("text", OBMessageUtils.messageBD("OBUIAPP_DeleteResult"));
      message.put("title", OBMessageUtils.messageBD("OBUIAPP_Success"));
      message.put("severity", "success");
      json.put("message", message);
      // and return it
      return json;
    } catch (Exception e) {
      // TODO:manage JDBCExceptionReporter
      OBDal.getInstance().rollbackAndClose();
      // create the result
      JSONObject json = new JSONObject();
      JSONObject message = new JSONObject();
      try {
        message.put("text", OBMessageUtils.messageBD("ProcessRunError"));
        message.put("title", OBMessageUtils.messageBD("error"));
        message.put("severity", "error");
        json.put("message", message);
      } catch (Exception ex) {
        throw new OBException("Error deleting remittance lines.", ex);
      }
      return json;
    }
  }
}