/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2013 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.process;

import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.module.remittance.RemittanceLineReturn;
import org.openbravo.scheduling.ProcessBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class REM_RemittanceProtestUndo implements org.openbravo.scheduling.Process {
  private static final Logger log4j = LoggerFactory.getLogger(REM_RemittanceProtestUndo.class);
  private static AdvPaymentMngtDao dao;
  public static final String STATUS_SENT = "REM_SENT";
  public static final String STATUS_AWAITINGEXECUTION = "RPAE";
  public static final String STATUS_RECEIVED = "RPR";

  public void execute(ProcessBundle bundle) throws Exception {
    dao = new AdvPaymentMngtDao();
    OBError msg = new OBError();
    msg.setType("Success");
    msg.setTitle(Utility.messageBD(bundle.getConnection(), "Success", bundle.getContext()
        .getLanguage()));

    OBContext.setAdminMode();
    try {
      final String recordID = (String) bundle.getParams().get("REM_RemittanceLine_Return_ID");
      final RemittanceLineReturn remittanceLineReturn = dao.getObject(RemittanceLineReturn.class,
          recordID);
      final VariablesSecureApp vars = bundle.getContext().toVars();
      final ConnectionProvider conProvider = bundle.getConnection();
      if ("Y".equals(remittanceLineReturn.getPosted())) {
        msg.setType("Error");
        msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
        msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
            "@PostedDocument@"));
        bundle.setResult(msg);
        return;
      }
      FIN_Payment payment = remittanceLineReturn.getPayment();
      if (!STATUS_AWAITINGEXECUTION.equals(payment.getStatus())
          && !STATUS_RECEIVED.equals(payment.getStatus())) {
        // Check lines not payment
        msg.setType("Error");
        msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
        msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
            "@REM_RemittanceReturnManaged@"));
        bundle.setResult(msg);
        return;
      }
      payment.setStatus(STATUS_SENT);
      OBDal.getInstance().save(payment);
      OBDal.getInstance().flush();
      remittanceLineReturn.setProcessed(false);
      OBDal.getInstance().save(remittanceLineReturn);
      OBDal.getInstance().flush();
      OBDal.getInstance().remove(remittanceLineReturn);
      OBDal.getInstance().flush();
      bundle.setResult(msg);
    } catch (final Exception e) {
      log4j.error("Error in execute: ", e);
      msg.setType("Error");
      msg.setTitle(Utility.messageBD(bundle.getConnection(), "Error", bundle.getContext()
          .getLanguage()));
      msg.setMessage(FIN_Utility.getExceptionMessage(e));
      bundle.setResult(msg);
      OBDal.getInstance().rollbackAndClose();
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
