/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012-2023 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.event;

import javax.enterprise.event.Observes;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.service.db.DalConnectionProvider;

public class PaymentEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = {
      ModelProvider.getInstance().getEntity(FIN_Payment.ENTITY_NAME) };

  @Override
  protected Entity[] getObservedEntities() {
    // TODO Auto-generated method stub
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final Entity finPaymentEntity = ModelProvider.getInstance().getEntity(FIN_Payment.ENTITY_NAME);
    final Property finPaymentStateProperty = finPaymentEntity
        .getProperty(FIN_Payment.PROPERTY_STATUS);
    final Property finPaymentProcessedProperty = finPaymentEntity
        .getProperty(FIN_Payment.PROPERTY_PROCESSED);
    final FIN_Payment payment = (FIN_Payment) event.getTargetInstance();
    String oldStatus = ((String) event.getPreviousState(finPaymentStateProperty));
    String newStatus = ((String) event.getCurrentState(finPaymentStateProperty));
    boolean newProcessed = (Boolean) (event.getCurrentState(finPaymentProcessedProperty));

    if ((!"RPAP".equalsIgnoreCase(oldStatus) && "RPAP".equalsIgnoreCase(newStatus))
        && (newProcessed == false)) {
      // @formatter:off
      String hql = ""
          + "select 1 "
          + " from REM_RemittanceLine "
          + " where payment.id = :id";
      // @formatter:on

      final Session session = OBDal.getInstance().getSession();
      @SuppressWarnings("rawtypes")
      final Query query = session.createQuery(hql);
      query.setParameter("id", payment.getId());
      query.setMaxResults(1);

      if (query.uniqueResult() != null) {
        String language = OBContext.getOBContext().getLanguage().getLanguage();
        ConnectionProvider conn = new DalConnectionProvider(false);
        throw new OBException(Utility.messageBD(conn, "REM_PaymentInRemittance", language));
      }
    }
  }
}
