/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2023 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.hooks;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.advpaymentmngt.ProcessInvoiceHook;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;

@ApplicationScoped
public class REMProcessInvoiceHook implements ProcessInvoiceHook {

  @Override
  public OBError preProcess(Invoice invoice, String strDocAction) {
    // In case of void a non paid invoice, check if there is a Remitted payment related.
    // Similar to APRM_InvoiceAwaitingExcutionPaymentRelated check done in #ProcessInvoice.java
    if ("RC".equals(strDocAction) && !invoice.isPaymentComplete().booleanValue()
        && invoice.getTotalPaid().compareTo(BigDecimal.ZERO) == 0) {

      // @formatter:off
      String hql = 
                "as fp" +
                "  join fp.fINPaymentDetailList fpd" +
                "  join fpd.fINPaymentScheduleDetailList fpsd" +
                "  join fpsd.invoicePaymentSchedule fps" +
                " where fps.invoice.id = :invoiceId" +
                "   and fp.status in ('REM_SENT')";
      // @formatter:on
      FIN_Payment payment = OBDal.getInstance()
          .createQuery(FIN_Payment.class, hql)
          .setNamedParameter("invoiceId", invoice.getId())
          .setMaxResult(1)
          .uniqueResult();

      if (payment != null) {
        final OBError msg = new OBError();
        msg.setType("Error");
        msg.setTitle(OBMessageUtils.messageBD("Error"));
        msg.setMessage(OBMessageUtils.messageBD("REM_InvoiceRemittedPaymentRelated"));
        return msg;
      }
    }

    return null;
  }

  @Override
  public OBError postProcess(Invoice invoice, String strDocAction) {
    return null;
  }

}
