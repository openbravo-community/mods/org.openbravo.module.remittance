/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.utility;

import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.module.remittance.Remittance;

public abstract class REM_RemittanceCreateFile {
  private Remittance remittance;
  OBError myError = null;
  String lineErrorDetail = "";
  String filename = "";

  public REM_RemittanceCreateFile() {
  }

  public REM_RemittanceCreateFile(Remittance _remittance) {
    init(_remittance);
  }

  public void init(Remittance _remittance) {
    remittance = _remittance;
  }

  /**
   * @return the myError
   */
  public OBError getMyError() {
    return myError;
  }

  /**
   * @param error
   *          the myError to set
   */
  public void setMyError(OBError error) {
    this.myError = error;
  }

  public void setMyError(String strMessage) {
    OBError error = new OBError();
    error.setType("Error");
    error.setTitle("Error");
    error.setMessage(strMessage);
    this.myError = error;
  }

  public void setLineErrorDetail(String detail) {
    this.lineErrorDetail = detail;
  }

  public String getLineErrorDetail() {
    return this.lineErrorDetail;
  }

  public void setRemittance(Remittance _remittance) {
    remittance = _remittance;
  }

  public Remittance getRemittance() {
    return remittance;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String rPad(String _toPadString, char c, int _size) {
    String toPadString = _toPadString;
    if (toPadString == null) {
      toPadString = "";
    }
    if (toPadString.length() > _size) {
      toPadString = toPadString.substring(0, _size);
    }
    int size = _size;
    StringBuffer toPad = new StringBuffer(toPadString);
    while (toPad.length() < size) {
      toPad.append(c);
    }
    return toPad.toString();
  }

  public String lPad(String _toPadString, char c, int _size) {
    String toPadString = _toPadString;
    if (toPadString == null) {
      toPadString = "";
    }
    if (toPadString.length() > _size) {
      toPadString = toPadString.substring(0, _size);
    }
    int size = _size;
    StringBuffer toPad = new StringBuffer();
    while (toPadString.length() < size) {
      toPad.append(c);
      size--;
    }
    return toPad.append(toPadString).toString();
  }

  public String parseText(String _toParse) {
    String toParse = _toParse;
    if (toParse == null || "".equals(toParse))
      return toParse;
    toParse = toParse.replace('Á', 'A');
    toParse = toParse.replace('á', 'a');
    toParse = toParse.replace('É', 'E');
    toParse = toParse.replace('é', 'e');
    toParse = toParse.replace('Í', 'I');
    toParse = toParse.replace('í', 'i');
    toParse = toParse.replace('Ó', 'O');
    toParse = toParse.replace('ó', 'o');
    toParse = toParse.replace('Ú', 'U');
    toParse = toParse.replace('ú', 'u');
    toParse = toParse.replace('Ñ', 'N');
    toParse = toParse.replace('ñ', 'n');
    return toParse;
  }

  public abstract StringBuffer generate(Remittance _remittance);

}
