/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.dao.TransactionsDao;
import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.advpaymentmngt.process.FIN_TransactionProcess;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.RemittanceLineCancel;
import org.openbravo.module.remittance.RemittanceLineReturn;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalConnectionProvider;

public class REM_SettleProtest {
  public static final String STATUS_CANCEL = "REM_CANCEL";
  public static final String STATUS_PAYMENTMADE = "PPM";
  public static final String STATUS_PAYMENTRECEIVED = "RPR";
  public static final String STATUS_AWAITING = "RPAE";

  public static void cancelPayment(FIN_Payment payment, Date dateAcct) throws Exception {
    ConnectionProvider conn = new DalConnectionProvider();
    VariablesSecureApp vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
        OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
            .getCurrentOrganization().getId(), OBContext.getOBContext().getRole().getId());
    boolean isAlreadyPaid = false;
    try {
      OBContext.setAdminMode();

      if (FIN_Utility.getFinAccTransaction(payment) != null) {
        throw new OBException("@REM_TransactionExists@");
      }

      Remittance remittance = getPaymentRemittance(payment);
      RemittanceLineCancel remCancel = OBProvider.getInstance().get(RemittanceLineCancel.class);
      remCancel.setClient(remittance.getClient());
      remCancel.setOrganization(remittance.getOrganization());
      remCancel.setRemittance(remittance);
      remCancel.setLineNo(getCancelMaxLineNo(remittance) + 10);

      OBCriteria<RemittanceLine> rmLineList = OBDal.getInstance().createCriteria(
          RemittanceLine.class);
      rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE, remittance));
      rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_PAYMENT, payment));
      rmLineList.setMaxResults(1);
      RemittanceLine remLine = (RemittanceLine) rmLineList.uniqueResult();

      if (remLine != null) {
        if (remittance.getRemittanceType().isRemitForDiscount()) {
          if (remLine.getOrigstatus() == null) {
            isAlreadyPaid = false;
          } else if (remLine.getOrigstatus().equals(STATUS_AWAITING)) {
            isAlreadyPaid = false;
          } else {
            isAlreadyPaid = (FIN_Utility.seqnumberpaymentstatus(remLine.getOrigstatus())) >= (FIN_Utility
                .seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine.getPayment())));
          }
        } else {
          if (remLine.getOrigstatus() == null) {
            isAlreadyPaid = false;
          } else if (remLine.getOrigstatus().equals(STATUS_AWAITING)) {
            isAlreadyPaid = false;
          } else {
            isAlreadyPaid = (FIN_Utility.seqnumberpaymentstatus(remLine.getOrigstatus())) <= (FIN_Utility
                .seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine.getPayment())));
          }
        }
      }
      if (remittance.getRemittanceType().isRemitForDiscount()) {
        payment.setStatus(STATUS_CANCEL);
      } else {
        payment.setStatus(payment.isReceipt() ? STATUS_PAYMENTRECEIVED : STATUS_PAYMENTMADE);
        payment.setPosted("N");
        OBDal.getInstance().save(payment);
        // OBDal.getInstance().flush();
        if (payment.getAmount().compareTo(BigDecimal.ZERO) != 0) {
          FIN_FinaccTransaction transaction = TransactionsDao.createFinAccTransaction(payment);
          transaction.setAccount(remittance.getFinancialAccount());
          transaction.setDateAcct(dateAcct);
          transaction.setTransactionDate(dateAcct);
          if (!StringUtils.equals(remittance.getFinancialAccount().getCurrency().getId(), payment
              .getCurrency().getId())) {
            transaction.setForeignCurrency(payment.getCurrency());
            BigDecimal convertedAmount = FinancialUtils.getConvertedAmount(payment.getAmount(),
                payment.getCurrency(), remittance.getFinancialAccount().getCurrency(), dateAcct,
                payment.getOrganization(), FinancialUtils.PRECISION_PRICE);
            BigDecimal depositAmt = FIN_Utility.getDepositAmount(payment.getDocumentType()
                .getDocumentCategory().equals("ARR"), convertedAmount);
            BigDecimal paymentAmt = FIN_Utility.getPaymentAmount(payment.getDocumentType()
                .getDocumentCategory().equals("ARR"), convertedAmount);

            transaction.setDepositAmount(depositAmt);
            transaction.setPaymentAmount(paymentAmt);
            BigDecimal txnConvRate = convertedAmount.divide(payment.getAmount(), 6,
                RoundingMode.HALF_UP);
            transaction.setForeignConversionRate(txnConvRate);
            transaction.setForeignAmount(payment.getAmount());
          }
          OBDal.getInstance().save(transaction);
          OBError processTransactionError = processTransaction(vars, conn, "P", transaction);
          if (processTransactionError != null
              && StringUtils.equals(processTransactionError.getType(), "Error")) {
            throw new OBException(processTransactionError.getMessage());
          }
        }
        if ((FIN_Utility.seqnumberpaymentstatus(payment.getStatus())) <= (FIN_Utility
            .seqnumberpaymentstatus(FIN_Utility.invoicePaymentStatus(remLine.getPayment())))) {
          isAlreadyPaid = true;
        }
      }

      remCancel.setAccountingDate(dateAcct);
      remCancel.setPayment(payment);
      OBDal.getInstance().save(remCancel);
      // OBDal.getInstance().flush();
      remCancel.setProcessed(true);
      if (!isAlreadyPaid) {
        // Update Payment Monitor and Customer Credit
        BusinessPartner businessPartner = payment.getBusinessPartner();
        boolean isReceipt = payment.isReceipt();
        // When credit is used (consumed) we compensate so_creditused as this amount is already
        // included in the payment details. Credit consumed should not affect to so_creditused
        if (payment.getGeneratedCredit().compareTo(BigDecimal.ZERO) == 0
            && payment.getUsedCredit().compareTo(BigDecimal.ZERO) != 0) {
          if (isReceipt) {
            increaseCustomerCredit(businessPartner, payment.getUsedCredit());
          } else {
            decreaseCustomerCredit(businessPartner, payment.getUsedCredit());
          }
        }
        for (FIN_PaymentDetail paymentDetail : payment.getFINPaymentDetailList()) {
          for (FIN_PaymentScheduleDetail paymentScheduleDetail : paymentDetail
              .getFINPaymentScheduleDetailList()) {
            BigDecimal amount = paymentScheduleDetail.getAmount().add(
                paymentScheduleDetail.getWriteoffAmount());
            if (paymentScheduleDetail.getInvoicePaymentSchedule() != null) {
              // BP SO_CreditUsed
              businessPartner = paymentScheduleDetail.getInvoicePaymentSchedule().getInvoice()
                  .getBusinessPartner();
              if (isReceipt) {
                decreaseCustomerCredit(businessPartner, amount);
              } else {
                increaseCustomerCredit(businessPartner, amount);
              }
              FIN_AddPayment.updatePaymentScheduleAmounts(paymentDetail,
                  paymentScheduleDetail.getInvoicePaymentSchedule(),
                  paymentScheduleDetail.getAmount(), paymentScheduleDetail.getWriteoffAmount());
            }
            if (paymentScheduleDetail.getOrderPaymentSchedule() != null) {
              FIN_AddPayment.updatePaymentScheduleAmounts(paymentDetail,
                  paymentScheduleDetail.getOrderPaymentSchedule(),
                  paymentScheduleDetail.getAmount(), paymentScheduleDetail.getWriteoffAmount());
            }
            // when generating credit for a BP SO_CreditUsed is also updated
            if (paymentScheduleDetail.getInvoicePaymentSchedule() == null
                && paymentScheduleDetail.getOrderPaymentSchedule() == null
                && paymentScheduleDetail.getPaymentDetails().getGLItem() == null) {
              // BP SO_CreditUsed
              if (isReceipt) {
                decreaseCustomerCredit(businessPartner, amount);
              } else {
                increaseCustomerCredit(businessPartner, amount);
              }
            }
            paymentScheduleDetail.setInvoicePaid(true);

            OBDal.getInstance().save(paymentScheduleDetail);
          }
        }
        OBDal.getInstance().save(payment);
        OBDal.getInstance().save(remCancel);
        OBDal.getInstance().flush();
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    isAlreadyPaid = false;
  }

  public static void returnPayment(FIN_Payment payment, String strDateAcct) {
    try {
      OBContext.setAdminMode();
      Remittance remittance = getPaymentRemittance(payment);
      RemittanceLineReturn remReturn = OBProvider.getInstance().get(RemittanceLineReturn.class);
      remReturn.setClient(remittance.getClient());
      remReturn.setOrganization(remittance.getOrganization());
      remReturn.setRemittance(remittance);
      remReturn.setLineNo(getReturnMaxLineNo(remittance) + 10);
      remReturn.setAccountingDate(FIN_Utility.getDate(strDateAcct));
      remReturn.setPayment(payment);
      OBDal.getInstance().save(remReturn);
      // OBDal.getInstance().flush();
      remReturn.setProcessed(true);
      OBCriteria<RemittanceLine> rmLineList = OBDal.getInstance().createCriteria(
          RemittanceLine.class);
      rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE, remittance));
      rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_PAYMENT, payment));
      if (rmLineList.list() != null) {
        if (rmLineList.list().get(0).getOrigstatus() == null) {
          payment.setStatus("RPAE"); // Awaiting execution
        } else {
          payment.setStatus(rmLineList.list().get(0).getOrigstatus());
        }

      }
      OBDal.getInstance().save(payment);
      OBDal.getInstance().save(remReturn);
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public static Remittance getPaymentRemittance(FIN_Payment payment) {
    OBCriteria<RemittanceLine> obc = OBDal.getInstance().createCriteria(RemittanceLine.class);
    obc.add(Restrictions.eq(RemittanceLine.PROPERTY_PAYMENT, payment));
    for (RemittanceLine line : obc.list()) {
      boolean iscanceled = false;
      for (RemittanceLineCancel cancel : line.getRemittance().getREMRemittanceLineCancelList()) {
        if (cancel.getPayment().getId().equals(payment.getId())) {
          iscanceled = true;
          break;
        }
      }
      if (iscanceled) {
        continue;
      }
      boolean isprotested = false;
      for (RemittanceLineReturn lineReturn : line.getRemittance().getREMRemittanceLineReturnList()) {
        if (lineReturn.getPayment().getId().equals(payment.getId())) {
          isprotested = true;
          break;
        }
      }
      if (isprotested) {
        continue;
      }
      return line.getRemittance();
    }
    throw new OBException("@REM_PaymentNoRemittance@");
  }

  public static Long getCancelMaxLineNo(Remittance remittance) {
    OBContext.setAdminMode();
    Long maxLine = 0l;
    try {
      final OBCriteria<RemittanceLineCancel> obc = OBDal.getInstance().createCriteria(
          RemittanceLineCancel.class);
      obc.add(Restrictions.eq(RemittanceLineCancel.PROPERTY_REMITTANCE, remittance));
      obc.addOrderBy(RemittanceLineCancel.PROPERTY_LINENO, false);
      obc.setMaxResults(1);
      final List<RemittanceLineCancel> fat = obc.list();
      if (fat.size() == 0)
        return 0l;
      maxLine = fat.get(0).getLineNo();
    } finally {
      OBContext.restorePreviousMode();
    }
    return maxLine;
  }

  private static Long getReturnMaxLineNo(Remittance remittance) {
    OBContext.setAdminMode();
    Long maxLine = 0l;
    try {
      final OBCriteria<RemittanceLineReturn> obc = OBDal.getInstance().createCriteria(
          RemittanceLineReturn.class);
      obc.add(Restrictions.eq(RemittanceLineReturn.PROPERTY_REMITTANCE, remittance));
      obc.addOrderBy(RemittanceLineReturn.PROPERTY_LINENO, false);
      obc.setMaxResults(1);
      final List<RemittanceLineReturn> fat = obc.list();
      if (fat.size() == 0)
        return 0l;
      maxLine = fat.get(0).getLineNo();
    } finally {
      OBContext.restorePreviousMode();
    }
    return maxLine;
  }

  private static void updateCustomerCredit(BusinessPartner businessPartner, BigDecimal amount,
      boolean add) {
    try {
      OBContext.setAdminMode();
      BigDecimal creditUsed = businessPartner.getCreditUsed();
      if (add) {
        creditUsed = creditUsed.add(amount);
      } else {
        creditUsed = creditUsed.subtract(amount);
      }
      businessPartner.setCreditUsed(creditUsed);
      OBDal.getInstance().save(businessPartner);
      // OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private static void increaseCustomerCredit(BusinessPartner businessPartner, BigDecimal amount) {
    updateCustomerCredit(businessPartner, amount, true);
  }

  private static void decreaseCustomerCredit(BusinessPartner businessPartner, BigDecimal amount) {
    updateCustomerCredit(businessPartner, amount, false);
  }

  /**
   * It calls the Transaction Process for the given transaction and action.
   * 
   * @param vars
   *          VariablesSecureApp with the session data.
   * @param conn
   *          ConnectionProvider with the connection being used.
   * @param strAction
   *          String with the action of the process. {P, D, R}
   * @param transaction
   *          FIN_FinaccTransaction that needs to be processed.
   * @return a OBError with the result message of the process.
   * @throws Exception
   */
  private static OBError processTransaction(VariablesSecureApp vars, ConnectionProvider conn,
      String strAction, FIN_FinaccTransaction transaction) throws Exception {
    ProcessBundle pb = new ProcessBundle("F68F2890E96D4D85A1DEF0274D105BCE", vars).init(conn);
    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("action", strAction);
    parameters.put("Fin_FinAcc_Transaction_ID", transaction.getId());
    pb.setParams(parameters);
    OBError myMessage = null;
    new FIN_TransactionProcess().execute(pb);
    myMessage = (OBError) pb.getResult();
    return myMessage;
  }
}
